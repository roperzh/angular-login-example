class FacebookController < ApplicationController
  # Just a hacky workaround to allow CORS
  before_filter :set_headers

  # Another hacky workaround to avoid auth tokens, just for the demo app
  skip_before_filter :verify_authenticity_token

  def fetch
    # The access token is sent as a parameter
    access_token = params["accessToken"]

    begin
      # Here I'm using the `fb_graph` gem, but it could be another gem
      # like `koala`, the main thing here is fetch the user with the proper
      # auth token
      user = FbGraph::User.me(access_token).fetch
    rescue => the_error
      @error = the_error.message
      puts "Error #{the_error.message}"
    end

    # If the user exist mean that the token is valid, we can proceed as normally
    if user && user.email
      # Create a new user if does not exist...
      # Login the user...
      # Send a response
      render json: { status: :ok }
    end
  end

  def options
    set_headers
    render :text => "", :content_type => "text/plain"
  end

  private

  def set_headers
    headers["Access-Control-Allow-Origin"] = "*"
    headers["Access-Control-Expose-Headers"] = "Etag"
    headers["Access-Control-Allow-Methods"] = "GET, POST, PUT, DELETE, PATCH, OPTIONS, HEAD"
    headers["Access-Control-Allow-Headers"] = "*, x-requested-with, Content-Type, If-Modified-Since, If-None-Match"
    headers["Access-Control-Max-Age"] = "86400"
  end
end
