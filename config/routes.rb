Rails.application.routes.draw do
  root "home#index"

  match "facebook/fetch", to: "facebook#fetch", via: [:get, :post]
  match "facebook/fetch", to: "facebook#options", :constraints => {:method => "OPTIONS"}, via: [:options]

end
